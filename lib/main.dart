import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  // This widget is the root of your application.


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'FlowerShopLab2',
      home: BottomNavBar(),
    );
  }

}

class flowerSheet extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => flowerSheetState();
}

class flowerSheetState extends State<flowerSheet> {
  var _flower = 'lib/assets/redFlower.png';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [

              Image.asset(_flower, width: 300,),
              Container(
                padding: EdgeInsets.only(top:50),
                child: Column(
                  children: [
                    Text("Welcome to the Flower Shop!",
                      style: TextStyle(fontSize: 22),),
                    RaisedButton(
                      child: Text("Browse Flowers"),
                      onPressed: displayBottomSheet,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void displayBottomSheet() {
    showModalBottomSheet<void> (
      backgroundColor: Colors.transparent,
      context: context,
      builder: (BuildContext context) {
        return Container(
          child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                ListTile(
                  leading: Icon(
                    Icons.brightness_low,
                    color: Colors.red,
                  ),
                  title: Text("Red Flower"),
                  onTap: () {
                    setState(() {
                      _flower = 'lib/assets/redFlower.png';
                    });
                  },
                ),
                ListTile(
                  leading: Icon(
                    Icons.brightness_medium,
                    color: Colors.blue,
                  ),
                  title: Text("Blue Flower"),
                  onTap: () {
                    setState(() {
                      _flower = 'lib/assets/blueFlower.png';
                    });
                  },
                ),
                ListTile(
                  leading: Icon(
                    Icons.brightness_high,
                    color: Colors.amber,
                  ),
                  title: Text("Yellow Flower"),
                  onTap: () {
                    setState(() {
                      _flower = 'lib/assets/yellowFlower.png';
                    });
                  },
                ),
              ],
            ),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20.0),
            ),
        );
      },
    );
  }
}

class BottomNavBar extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => BottomNavBarState();
}

class BottomNavBarState extends State<BottomNavBar> {

  int _selectedIndex = 0;
  static const TextStyle navBarTextStyle = TextStyle(
      fontSize: 30,
      fontWeight: FontWeight.bold);
  static List<Widget> _navBarButtons = <Widget>[
    flowerSheet(),
    descWidget(),
  ] ;

  @override
  Widget build(BuildContext context) {
    return _bottomNavigationBar();
  }

  Scaffold _bottomNavigationBar() {
    return Scaffold(
      appBar: AppBar(
        title: Text("Flower Shop Lab 2"),
        backgroundColor: Colors.lightBlueAccent,
      ),
      body: Center(
        child: _navBarButtons.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.lightBlueAccent,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('Flowers'),),
            BottomNavigationBarItem(icon: Icon(Icons.book), title: Text('Description'),),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.black26,
          onTap: _onNavButtonTapped,
      ),
    );
  }

  void _onNavButtonTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}

class descWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => descWidgetState();
}

class descWidgetState extends State<descWidget> {

  @override
  Widget build(BuildContext context) {

    Widget textSection = Container(
      padding: const EdgeInsets.all(15),
      child: Text(
        'We\'ve got a wide selection of flowers available! Sadly we are unable to get everything listed '
        'on the website, but we have three types of a Potentilla in the three primary colors! '
        'Keep in touch for future updates on our wide array of flowers available online soon!',
        softWrap: true,
        style: TextStyle(fontSize: 15),
      ),
    );

    return Center(
      child: Column(
        children: [
          Image.asset('lib/assets/acFlowerField.png'),
          SizedBox(height: 10,),
          Container(
            padding: EdgeInsets.all(10),
            child: Text('Welcome to the Flower Shop!', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),),
            decoration: BoxDecoration(
              color: Color(0xFFF8F1D7),
              borderRadius: BorderRadius.circular(20.0),
              border: Border.all(
                color: Colors.amber,
                width: 3,
              ),
            ),
          ),
          textSection,
        ],
      ),
    );
  }
}
